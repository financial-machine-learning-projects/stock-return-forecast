# Stock Return Forecast 

In this case study we will use various supervised learning-based models to predict a stock return using correlated assets and its own historical data.

## 1. Problem Definition

In the supervised regression framework used for this case study, weekly return of a financial asset (stock) is the predicted variable. We need to understand what affects the return and hence incorporate as much information into the model.

For this case study, other than the historical data of Microsoft, the independent variables used are the following potentially correlated assets:
- **Stocks**: IBM (IBM), Alphabet (GOOGL)
- **Currency**: USD/JPY, GBP/USD
- **Indices**: S&P 500, Dow Jones and VIX

