import numpy as np
import pandas as pd
import pandas_datareader as pdr

from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

import src.settings.base as stg
from src.infrastructure.datasetcreation import DataFrameBuilder

class DatasetTransformer:
    
    """ Standardize numerical data and process to pipeline. 

    Attributes
    ----------
    
    Methods
    -------

    """

    def __init__(self):
        """initialise class

        Parameters
        ----------
        
        """
    @property
    def preprocessor(self):
        
        pipeline = Pipeline(steps=[(("scaler", StandardScaler()))])

        return pipeline